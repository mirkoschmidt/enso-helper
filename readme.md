# Laravel helper

A package with some simple helpers for laravel and bootstrap.

## Requirements
- PHP >= 5.6
- Laravel >= 5.3

## Installation

Add the package to your  `composer.json`:
```
composer require mirkoschmidt/enso-helper
```

## Setup

Add this line to the providers array in `config/app.php`:
```
Enso\Helper\HelperServiceProvider::class,
```

## Usage

### Active menu helper

Use the `active($routes, $onlyClass)` helper function within your blade templates where you want to add the `active` css class.

- Set the `$routes` parameter to any route you want to check, or an array of multiple routes. You can also use the `*` wilcard or the laravel route() function.
- Set the `$onlyClass` parameter to false, if you not only want to get the string "active", but also the whole class markup.

Example with bootstrap 4 markup:
```
<li class="nav-item{{ active(['profile', 'profile/*']) }}">
  <a class="nav-link" href="{{ url('profile') }}">Your profile</a>
</li>
```

### Error markup helper

Use the `error($errors, $name)` helper function within your blade templates, to check for errors on a specific
field and add the error message markup (bootstrap 4 form-control-feedback) if necessary.
 
 - Set the `$errors` parameter to your current views error message bag (`$errors` by default).
 - Set the `$name` parameter to the field name you want to check.
 
Example:
```
<div class="form-group row{{ $errors->has('name') ? ' has-danger' : '' }}">
  <label for="first_name" class="col-sm-2 col-form-label">{{ __('Name') }}</label>
  <div class="col-sm-10">
    <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
    {!! error($errors, 'name') !!}
  </div>
</div>
 ```
 
### Asset hash helper
 
Use the `hashed_asset($url, $default = null, $file = null)` helper function to append a hash to your asset based on 
the files last modification time.  
This is useful if you want to invalidate the users browser cache if the asset changes (e.g. if a user uploads a new profile picture).  
- Set the `$url` parameter to your assets relative url (like you would do with Laravels default `asset()` function).
- Set the `$default` parameter to a fallback value which will be returned if the asset file is not found (e.g. a placeholder avatar if a user not uploaded an own profile picture).  
**If you want to return a default asset, you have to use the asset() function in this parameter.** This is due to the fact, that you can also set a non-file value as the default value, e.g. an inline data image source.  
- Set the `$file` parameter to the path of the file from which the hash should be generated. 
This parameter is optional. If its not set, the function will try to find the file based on the url within the `public_path()`.

Example:
```
<img src="{{ hashed_asset('images/users/user-1.jpg') }}"/>
```
Will become:
```
<img src="https://your-domain.net/images/users/user-1.jpg?hash=1493372067">
```

### Validation rule "alpha_space"
 
You can use the additional validation rule `alpha_space` to make sure that an input contains only letters and spaces.

### SRI (subresource integrity hash) helper

Use the `sri($assetFile)` helper to dynamically calculate the integrity hash for your assets, which is important for a good security ranking of your web app.  
- Set the `$assetFile` parameter to the path of your asset file.

The hash will automatically recalculate when the modification date of your asset changes, and gets cached for better performance.

Example:
```
<script src="{{ mix('js/app.js') }}" integrity="{{ sri(public_path() . mix('js/app.js')) }}"></script>
```

## Author

Daniel Hoffmann | [Enso](http://enso.re)